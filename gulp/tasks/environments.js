'use strict';

import config       from '../config';
import gulp         from 'gulp';
import browserSync  from 'browser-sync';
import gulpNgConfig from 'gulp-ng-config';
import yargs        from 'yargs';
import through        from 'through2';

gulp.task('environments', function() {
  
  var env = () => {
  	if (!yargs.argv.env) return global.isProd ? 'prod' : 'local';
  	return yargs.argv.env;
  };

  return gulp.src(config.environments.src)
    .pipe(through.obj((file, enc, cb) => {
      var jsonObj = JSON.parse(file.contents.toString('utf8'));
      global.environment = jsonObj[env()].config;
      cb(null, file);
    }))
    .pipe(gulpNgConfig('app.config', {
    	environment: env()
    }))
    .pipe(gulp.dest(config.environments.dest))
    .pipe(browserSync.stream());

});
