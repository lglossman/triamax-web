import _ from 'lodash';

function CreateRaceCtrl($rootScope, $scope, RaceService, $state, $uibModalInstance, $translate) {
  'ngInject';

  $scope.pending = {
  	race: {}
  };

  $scope.types = [];
  $scope.countries = [];
	$scope.cities = [];
  
	$scope.filterCities = () => {
		$scope.pending.race.ciudad = null;
		$scope.cities = _.uniqBy( // remove duplicates
                        (
                          _.find($scope.countries, c => c == $scope.pending.race.pais) // cities for the country 
                          || // or
                          { // all cities if no country is selected
                            cities:_.flatMap($scope.countries, c => c.cities)
                          }
                        ).cities.sort(), c => c);
	};

  RaceService.filters().then(filters => {
  	$scope.types = filters.eventTypes;
  	$scope.countries = filters.countries;
  	$scope.filterCities();
  }, error => {
  	showError(error);
  });

	var today = new Date();
	today = new Date(today.getYear(), today.getMonth(), today.getDay());
	$scope.dateOptions = {
		formatYear: 'yy',
		showWeeks: false,
		minDate: new Date(),
		startingDay: 1
	};
	$scope.formats = ['dd/MM/yyyy'];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.open = () => {
		$scope.popup.opened = true;
	};
	$scope.popup = {
		opened: false
	};

  $scope.submit = () => {
  	if (!$scope.createForm.$valid) {
  		showError($translate.instant('MISSING_INFO'));
  		return;
  	}
  	if ($scope.pending.race.web) {
  		if (!$scope.pending.race.web.trim().toUpperCase().startsWith('HTTP://')
  			&& !$scope.pending.race.web.trim().toUpperCase().startsWith('HTTPS://')) {
  			$scope.pending.race.web = 'http://' + $scope.pending.race.web;
  		}
  	}
  	var pending = _.cloneDeep($scope.pending);
  	pending.race.nombreEvento = pending.race.nombreCarrera;
  	var distances = _.filter(_.map(pending.race.distancias.split(','), _.parseInt), v => v != NaN);
  	
  	pending.race.distancias = distances;
  	pending.race.fechaFin = pending.race.fechaInicio;

  	if (pending.race.pais) pending.race.pais = pending.race.pais.name;
		RaceService.createPending(pending).then(race => {
  		$rootScope.notify($translate.instant('RACE_SENT'));
  		$uibModalInstance.close();
  	}, error => {
  		showError(error);
  	});
  };

  $scope.cancel = () => {
  	$uibModalInstance.dismiss();
  };

	// TODO hacer generico
	$scope.alerts = [];
	function showError(error) {
		clearAlerts();
    addAlert({
      type: 'danger',
      msg: error
    });
	}
	function showAlert(data) {
    clearAlerts();
    addAlert(data);
	}
	function addAlert(data) {
    $scope.alerts.push(data);
	}
	function clearAlerts() {
    $scope.alerts.splice(0, $scope.alerts.length)
	}
	function closeAlert(index) {
    $scope.alerts.splice(index, 1);
	}
	$scope.addAlert = addAlert;
	$scope.closeAlert = closeAlert;
	$scope.showAlert = showAlert;
	$scope.showError = showError;

}

export default {
  name: 'CreateRaceCtrl',
  fn: CreateRaceCtrl
};
