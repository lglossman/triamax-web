import _ from 'lodash';
import Rx from 'rx';

function RacesCtrl($rootScope, $scope, RaceService, $state, $stateParams, UserService, $window) {
  'ngInject';
  
  $rootScope.bodyClass = 'light-bg';
  $scope.races = $stateParams.races;
  $scope.favorites = $stateParams.favorites == 'true';
  $scope.searchTerm = $stateParams.searchTerm;
  $scope.previousPage = parseInt($stateParams.page) - 1;
  $scope.nextPage = parseInt($stateParams.page) + 1;

  $scope.types = ['all'];
  $scope.months = ['all', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  $scope.distances = ['all', '1to5', '6to10', '11to15', '16to21', '22to42', '43'];
  $scope.countries = ['COUNTRIES_all'];
  $scope.cities = ['CITIES_all'];

  $scope.month = $stateParams.month ? parseInt($stateParams.month) : $scope.months[0];
  $scope.distance = $stateParams.distance ? $stateParams.distance : $scope.distances[0];
  $scope.minDistance = null;
  $scope.maxDistance = null;
  if (filterValue($scope.distance)) {
    var distances = $scope.distance.split('to');
    $scope.minDistance = distances[0];
    $scope.maxDistance = distances[1];
  }

  $scope.type = $scope.types[0];
  $scope.country = $scope.countries[0];
  $scope.city = $scope.cities[0];
  
  // load filters from server
  Rx.Observable.fromPromise(RaceService.filters()).flatMap(filters => {
    console.log('filters loaded');
    $scope.types = _.concat($scope.types, filters.eventTypes);
    $scope.type = $stateParams.type ? $stateParams.type : $scope.types[0];
    
    // show only countries with cities
    $scope.countries = _.concat($scope.countries, _.map(_.filter(filters.countries, c => c.cities.length > 0), c => c.name));
    $scope.country = $stateParams.country ? $stateParams.country : $scope.countries[0];

    $scope.cities = _.uniqBy( // remove duplicates
                      _.concat( // Default 'All' item with
                        $scope.cities, (
                          _.find(filters.countries, c => c.name == $scope.country) // cities for the country 
                          || // or
                          { // all cities if no country is selected
                            cities:_.flatMap(filters.countries, c => c.cities)
                          }
                        ).cities.sort()
                      ), c => c);
    $scope.city = $stateParams.city ? $stateParams.city : $scope.cities[0];

    // Favorites filter is not enabled
    if (!$scope.favorites) return Rx.Observable.just(null);
    
    console.log('getting user for favorites');
    // get user
    return UserService.getCurrentUser()
      .map(user => user.username) // user is present
      .catch(error => Rx.Observable.just(null)); // no user logged in
  }).flatMap(username => {
    if (username) console.log('getting races for ' + username);
    
    // if races already loaded
    if ($scope.races) return Rx.Observable.just($scope.races);
    
    return RaceService.find({ // then load races
      searchTerm: $scope.searchTerm,
      type: filterValue($scope.type),
      month: filterValue($scope.month),
      minDistance: $scope.minDistance,
      maxDistance: $scope.maxDistance,
      country: filterValue($scope.country),
      city: filterValue($scope.city),
      page: $stateParams.page,
      username: username
    });
  }).subscribe(races => {
    console.log('races loaded');
    $scope.races = races;
  }, error => {
    console.log('some error in chain: ' + JSON.stringify(error));
    $rootScope.notify(error);
  });

  const didFilterChange = () => {
    return $scope.type != $stateParams.type 
      || $scope.month != $stateParams.month
      || $scope.distance != $stateParams.distance
      || $scope.country != $stateParams.country
      || $scope.city != $stateParams.city;
  };

  $scope.onCountryChanged = () => {
    if (didFilterChange()) {
      $scope.city = null; // reset city
      $scope.onFilterChanged();
    }
  };

  // Events
  $scope.onFilterChanged = () => {
    if (didFilterChange()) {
      $scope.search();
    }
  };

  $scope.search = () => {
    // reload state with different search term
  	$state.go('Races', {
  		searchTerm: $scope.searchTerm,
      type: filterValue($scope.type),
      month: filterValue($scope.month),
      distance: filterValue($scope.distance),
      country: filterValue($scope.country),
      city: filterValue($scope.city),
  		page: 0,
      races: null
  	});
  };

  $scope.noResults = () => {
    if ($scope.favorites) {
      return "No tenes ninguna carrera guardada";
    } else {
      if ($stateParams.searchTerm && $stateParams.searchTerm.trim() != '') {
        return 'No se han encontrado resultados para <strong>' + $stateParams.searchTerm + '</strong>. Intenta con otras palabras clave';
      } else {
        return 'No se han encontrado resultados. Intenta con otras palabras clave';
      }
    }
  };

  function filterValue(filter) {
    if (!filter || filter == 'all' || (filter + '').indexOf('_all') != -1) return null;
    return filter;
  }

  $scope.toggleFilters = () => {
    $('.page-sidebar').slideToggle();
  };

  $scope.toggleFavorite = race => {
    UserService.getCurrentUser().subscribe(user => {
      if (race.favorite) {
        return RaceService.removeFavorite(user, race).then(newFavorites => {
          user.favorites = newFavorites;
          race.favorite = false;
        });
      } else {
        return RaceService.addFavorite(user, race).then(newFavorites => {
          user.favorites = newFavorites;
          race.favorite = true;
        });
      }
    }, error => {
      // No user logged in
      $rootScope.openLogin();
    });
  };

  $scope.handleRaceClick = race => {
    $window.open(race.web);
  };

  $scope.toggleFavoritesFilter = () => {
    UserService.getCurrentUser().subscribe(user => {
      $state.go('Races', {
        searchTerm: null,
        type: null,
        month: null,
        distance: null,
        country: null,
        city: null,
        page: 0,
        races: null,
        favorites: !$scope.favorites + ''
      });
    }, error => {
      // No user logged in
      $rootScope.openLogin();
    });
  };

  $scope.$on('logout', user => {
    $state.go('Home');
  });

}

export default {
  name: 'RacesCtrl',
  fn: RacesCtrl
};
