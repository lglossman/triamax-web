function HomeCtrl($rootScope, $scope, RaceService, $state, UserService) {
  'ngInject';

  $rootScope.bodyClass = '';
  $scope.searchTerm = '';

  $scope.search = () => {
		RaceService.find({
			searchTerm: $scope.searchTerm,
			page: 0
		}).then(data => {
			$state.go('Races', {
				searchTerm: $scope.searchTerm,
				page: 0,
				races: data
			});
		}, $rootScope.notify);
  };

  $scope.showFavorites = () => {
  	UserService.getCurrentUser().subscribe(user => {
      $state.go('Races', {
        searchTerm: null,
        type: null,
        month: null,
        distance: null,
        country: null,
        city: null,
        page: 0,
        races: null,
        favorites: 'true'
      });
    }, error => {
      // No user logged in
      $rootScope.openLogin();
    });
  };

}

export default {
  name: 'HomeCtrl',
  fn: HomeCtrl
};
