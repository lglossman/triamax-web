function OnConfig($stateProvider, $locationProvider, $urlRouterProvider, $translateProvider, nyaBsConfigProvider, $httpProvider) {
  'ngInject';

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  // defaults to JSON for everything
  $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';

  $stateProvider
    .state('Home', {
      url: '/',
      controller: 'HomeCtrl',
      templateUrl: 'home.html',
      title: 'Home'
    })
    .state('Home.Approve', {
      url: 'approve?id',
      onEnter: ($rootScope, $state, $uibModal, $stateParams, RaceService, ErrorHandler) => {
        $uibModal.open({
            templateUrl: 'approve_race.html',
            controller: 'ApproveRaceCtrl',
            windowTemplateUrl: 'window.html',
            windowClass: 'custom',
            title: 'Aprobar Carrera',
            backdrop: 'static',
            size: 'md',
            resolve: {
              pendingRace: () => new Promise((resolve, reject) => {
                RaceService.getPending($stateParams.id).then(resolve, error => {
                  $rootScope.notify(ErrorHandler.handle(error, 'approve'));
                  reject(error);
                });
              }),
              filters: () => new Promise((resolve, reject) => {
                RaceService.filters().then(resolve, error => {
                  $rootScope.notifications = [error];
                  reject(error);
                });
              })
            }
        }).result.finally(() => {
            $state.go('^');
        });
      }
    })
    .state('Races', {
      url: '/races?searchTerm&page&type&month&distance&country&city&favorites',
      params: {
          races: null,
          favorites: 'false'
      },
      controller: 'RacesCtrl',
      templateUrl: 'races.html',
      title: 'Resultados'
    });

  $urlRouterProvider.otherwise('/');

  var translations = {
    SLOGAN: 'Todas las carreras en un solo lugar',
    SEARCH_PLACEHOLDER: 'Busca una carrera...',
    NO_RESULTS: 'No se encontraron carreras.',
    NEWER: 'Adelante',
    OLDER: 'Atrás',
    RACES: 'Carreras',
    TRAINERS: 'Entrenadores',
    TEAMS: 'Teams',
    MENU: 'Menú',
    FILTERS: 'Filtros',
    EVENT_TYPE: 'Disciplina',
    EVENT_TYPE_all: 'Todas',
    EVENT_TYPE_CARRERA_DE_CALLE: 'Carrera de calle',
    EVENT_TYPE_TRIAL: 'Trial Running',
    EVENT_TYPE_TRIATLON: 'Triatlón',
    EVENT_TYPE_DUATLON: 'Duatlón',
    EVENT_TYPE_AVENTURA: 'Aventura',
    EVENT_TYPE_OBSTACULOS: 'Obstáculos',
    CARRERA_DE_CALLE: 'Carrera de calle',
    TRIAL: 'Trial Running',
    TRIATLON: 'Triatlón',
    DUATLON: 'Duatlón',
    AVENTURA: 'Aventura',
    OBSTACULOS: 'Obstáculos',
    MONTH: 'Mes',
    DISTANCE: 'Distancia',
    COUNTRY: 'País',
    CITY: 'Ciudad',
    MONTH_all: 'Todos',
    MONTH_1: 'Enero',
    MONTH_2: 'Febrero',
    MONTH_3: 'Marzo',
    MONTH_4: 'Abril',
    MONTH_5: 'Mayo',
    MONTH_6: 'Junio',
    MONTH_7: 'Julio',
    MONTH_8: 'Agosto',
    MONTH_9: 'Septiembre',
    MONTH_10: 'Octubre',
    MONTH_11: 'Noviembre',
    MONTH_12: 'Diciembre',
    DISTANCE_all: 'Todas',
    DISTANCE_1to5: '1 a 5k',
    DISTANCE_6to10: '6 a 10k',
    DISTANCE_11to15: '11 a 15k',
    DISTANCE_16to21: '16 a 21k',
    DISTANCE_22to42: '22 a 42k',
    DISTANCE_43: '+42k',
    COUNTRIES_all: 'Todos',
    CITIES_all: 'Todas',
    PICK_TYPE: 'Disciplina',
    PICK_COUNTRY: 'Elige el país',
    PICK_CITY: 'Elige la ciudad',
    SEND_DATA: 'Enviar datos',
    PERSONAL_DATA: 'Tus datos',
    RACE_NAME: 'Nombre de la carrera',
    CLOSE: 'Cerrar',
    TODAY: 'Hoy',
    DELETE: 'Borrar',
    DISTANCES: 'Distancias',
    DISTANCES_PLACEHOLDER: 'Distancias (Ej:40,30,40)',
    WEBSITE: 'Sitio web',
    OPTIONAL_RACE_EMAIL: 'Email de la carrera (opcional)',
    START_DATE: 'Fecha de inicio',
    ADD_RACE: 'Agrega una carrera',
    NAME: 'Nombre',
    EMAIL: 'Email',
    MISSING_INFO: 'Estos campos son obligatorios.',
    WRONG_EMAIL: 'Ingrese una dirección de correo válida',
    'invalid_grant': 'El usuario o la contraseña ingresada no son correctos.',
    'network_error': 'Por favor compruebe su conexión a internet.',
    'http.400': 'Compruebe los datos ingresados.',
    'http.401': 'Su sesión ha caducado, vuelva a ingresar.',
    'http.403': 'Su sesión ha caducado, vuelva a ingresar.',
    'http.404': 'No se encontraron resultados.',
    'http.404.approve': 'La carrera ya fue aprobada o eliminada.',
    'http.500': 'Ha ocurrido un error.',
    'pending.email': 'El email ingresado no es correcto.',
    'pending.name': 'Ingrese su nombre.',
    'pending.recaptchaResponse': 'Debe validar el captcha.',
    'race.nombreCarrera': 'El nombre de la carrera debe tener al menos 3 letras.',
    'race.fechaInicio': 'Ingrese una fecha de inicio ingresada no es válida.',
    'race.distancias': 'Ingresa entre 1 y 3 distancias.',
    'race.tipoEvento': 'Seleccione la disciplina.',
    'race.pais': 'Seleccione el país.',
    'race.ciudad': 'La ciudad ingresada no es válida.',
    'race.web': 'Ingrese el sitio web de la carrera.',
    'race.mail': 'El mail de la carrera no es correcto.',
    'invite.emails': 'Ingrese al menos un email.',
    EDIT_RACE: 'Editar Carrera',
    ACCEPT_PENDING_RACE: 'Agregar carrera',
    DELETE_PENDING_RACE: 'Rechazar carrera',
    RACE_SENT: 'Gracias por tu aporte, enviaremos la carrera para su revisión.',
    RACE_APPROVED: 'La carrera ha sido aprobada',
    RACE_REJECTED: 'La carrera ha sido rechazada',
    ADD_RACE_FOOTER: 'Agregar carrera',
    LOGIN: 'Ingresar',
    LOGOUT: 'Salir',
    LOGIN_TEXT: 'Somos la comunidad deportiva más grande de Latinoamérica',
    LOGIN_FACEBOOK: 'Iniciar Sesión con Facebook',
    SIGNUP: 'Registrarse',
    INVITE_FRIENDS: 'Invitar amigos',
    INVITE_TEXT: 'Invita a tus amigos a conocer nuevas carreras.',
    INVITE: 'Invitar',
    OMIT: 'Omitir',
    INVITE_SUCCESS: 'Gracias por recomendarnos!'
  };

  // add translation table
  $translateProvider.translations('es', translations);
  $translateProvider.preferredLanguage('es');

  nyaBsConfigProvider.setLocalizedText('es', {
    defaultNoneSelection: 'TODOS',
    noSearchResult: 'SIN RESULTADOS'
  });
  nyaBsConfigProvider.useLocale('es');
}

export default OnConfig;
