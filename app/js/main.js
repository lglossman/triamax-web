import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import angular from 'angular';
import 'angular-i18n/es-ar';
import 'angular-ui-router';
import 'angular-sanitize';
import 'angular-recaptcha';
import 'angular-loading-bar';
import 'angular-ui-bootstrap';
import 'angular-loading-bar/build/loading-bar.css';
import 'ngstorage'
import 'angular-translate';
import 'angular-animate';
import 'angular-growl-notifications';
import 'nya-bootstrap-select-up';
import 'nya-bootstrap-select-up/dist/css/nya-bs-select.css';
import 'flexibility';
import 'oclazyLoad';

// angular modules
import constants from './constants';
import onConfig  from './on_config';
import onRun     from './on_run';
import './environments';
import './templates';
import './filters';
import './controllers';
import './services';
import './directives';

// create and bootstrap application
const requires = [
  'ui.router',
  'ui.bootstrap',
  'templates',
  'app.filters',
  'app.controllers',
  'app.services',
  'app.directives',
  'angular-loading-bar',
  'pascalprecht.translate',
  'ngAnimate',
  'nya.bootstrap.select',
  'ngSanitize',
  'ngStorage',
  'app.config',
  'vcRecaptcha',
  'growlNotifications',
  'oc.lazyLoad'
];

// mount on window for testing
window.app = angular.module('app', requires);

angular.module('app').constant('AppSettings', constants);

angular.module('app').config(onConfig);

angular.module('app').run(onRun);

angular.bootstrap(document, ['app'], {
  strictDi: true
});
