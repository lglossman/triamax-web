import _ from 'lodash';

function OnRun($rootScope, AppSettings, config, $window, UserService, $uibModal, $ocLazyLoad) {
  'ngInject';

  $rootScope.config = config;

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.pageTitle = '';

    if ( toState.title ) {
      $rootScope.pageTitle += toState.title;
      $rootScope.pageTitle += ' \u2014 ';
    }

    $rootScope.pageTitle += AppSettings.appTitle;
  });

  $rootScope.notify = msg => {
    $rootScope.notifications = [{
      msg: msg,
      id: Math.random()
    }];
  };

  $rootScope.openLogin = title => {
    var modal = $uibModal.open({
      templateUrl: 'login.html',
      windowTemplateUrl: 'window.html',
      windowClass: 'custom login',
      controller: ($uibModalInstance, $scope) => {

        $scope.title = title || 'LOGIN';

        $scope.cancel = () => {
          $uibModalInstance.dismiss();
        };

        $scope.$on('login', (event, data) => {
          $uibModalInstance.dismiss();
          if (!data.existent) {
            $scope.openInvite();
          }
        });

      },
      backdrop: 'static',
      size: 'md'
    });
  };
  
  $rootScope.openSignup = () => {
    var modal = $uibModal.open({
      templateUrl: 'signup.html',
      windowTemplateUrl: 'window.html',
      windowClass: 'custom login',
      controller: ($uibModalInstance, $scope) => {

        $scope.title = 'SIGNUP';

        $scope.cancel = () => {
          $uibModalInstance.dismiss();
        };

        $scope.$on('login', (event, data) => {
          $uibModalInstance.dismiss();
          if (!data.existent) {
            $scope.openInvite();
          }
        });

      },
      backdrop: 'static',
      size: 'md'
    });
  };

  // TODO move to its own controller file
  $rootScope.openInvite = title => {
    var modal = $uibModal.open({
      templateUrl: 'invite.html',
      windowTemplateUrl: 'window.html',
      windowClass: 'custom invite',
      controller: ($uibModalInstance, $scope, $http, UserService, config, $translate, ErrorHandler) => {

        $scope.emails = [];
        
        var active = false;
        $scope.invite = () => {
          if (active) return;
          if (!$scope.createForm.$valid) {
            showError($translate.instant('WRONG_EMAIL'));
            return;
          }
          active = true;
          UserService.getCurrentUser().map(user => user.username).catch(error => null)
          .flatMap(username => {
            return $http({
              method: 'POST',
              data: {
                username: username,
                emails: _.compact($scope.emails)
              },
              url: config.endpoint + '/invite',
            });
          })
          .subscribe(response => {
            $rootScope.notify($translate.instant('INVITE_SUCCESS'));
            $uibModalInstance.dismiss();
          }, error => {
            showError(ErrorHandler.handle(error));
            active = false;
          });
        };

        $scope.cancel = () => {
          $uibModalInstance.dismiss();
        };

        // TODO hacer generico
        $scope.alerts = [];
        function showError(error) {
          clearAlerts();
          addAlert({
            type: 'danger',
            msg: error
          });
        }
        function showAlert(data) {
          clearAlerts();
          addAlert(data);
        }
        function addAlert(data) {
          $scope.alerts.push(data);
        }
        function clearAlerts() {
          $scope.alerts.splice(0, $scope.alerts.length)
        }
        function closeAlert(index) {
          $scope.alerts.splice(index, 1);
        }
        $scope.addAlert = addAlert;
        $scope.closeAlert = closeAlert;
        $scope.showAlert = showAlert;
        $scope.showError = showError;

      },
      backdrop: 'static',
      size: 'md'
    });
  };
  
  $rootScope.login = () => {
    UserService.login();
  };

  $rootScope.logout = () => {
    UserService.logout();
  };

  $rootScope.suggest = () => {
    var modal = $uibModal.open({
      templateUrl: 'create_race.html',
      windowTemplateUrl: 'window.html',
      windowClass: 'custom',
      controller: 'CreateRaceCtrl',
      backdrop: 'static',
      size: 'md',
      resolve: {
        deps: [() => {
            return $ocLazyLoad.load({
                insertBefore: '#ng_load_plugins_before',
                files: [
                    'https://www.google.com/recaptcha/api.js?hl=ES&onload=vcRecaptchaApiLoaded&render=explicit'
                ]
            });
        }]
      }
    });
  };

  $rootScope.blur = event => {
    setTimeout(() => {
      event.target.blur();
    }, 250)
  };
  
  $rootScope.isTouch = ('ontouchstart' in window);
  console.log('Is mobile: ' + $rootScope.isTouch);
  
  $rootScope.safeApply = function(fn) {
    var phase = this.$root.$$phase;
    if(phase == '$apply' || phase == '$digest') {
      if(fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };
  
}

export default OnRun;
