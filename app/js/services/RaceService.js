import _ from 'lodash';

function RaceService($http, config, ErrorHandler, UserService) {
  'ngInject';

  const service = {};

  service.find = filter => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'GET',
        params: filter,
        paramSerializer: '$httpParamSerializerJQLike',
        url: config.endpoint + '/races/find',
      }).then(data => {
        const page = data.data;
        UserService.getCurrentUser().subscribe(user => {
          _.forEach(page.list, r => {
            r.favorite = _.includes(user.favorites, r.idCarrera);
          });
          resolve(page);
        }, error => {
          _.forEach(page.list, r => {
            r.favorite = false;
          });
          resolve(page);
        });
        
      }, error => reject(ErrorHandler.handle(error)));
    });
  };

  var cachedFilters = null;

  service.filters = () => {
    return new Promise((resolve, reject) => {
      if (cachedFilters) {
        resolve(cachedFilters);
        return;
      }
      $http({
        method: 'GET',
        url: config.endpoint + '/races/filters',
      }).then(data => {
        cachedFilters = data.data;
        resolve(data.data);
      }, error => reject(ErrorHandler.handle(error)));
    });
  };

  service.createPending = pending => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'POST',
        data: pending,
        url: config.endpoint + '/races/pending',
      }).then(data => resolve(data.data), error => reject(ErrorHandler.handle(error)));
    });
  };

  service.getPending = id => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'GET',
        url: config.endpoint + '/races/pending/' + id,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(data => resolve(data.data), reject);
    });
  };

  service.approvePending = pendingRace => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'POST',
        url: config.endpoint + '/races/pending/approve',
        data: pendingRace
      }).then(data => {
        cachedFilters = null;
        resolve(data.data);
      }, reject);
    });
  };

  service.deletePending = id => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'DELETE',
        url: config.endpoint + '/races/pending/' + id,
        paramSerializer: '$httpParamSerializerJQLike'
      }).then(data => resolve(data.data), reject);
    });
  };

  service.addFavorite = (user, race) => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'POST',
        url: config.endpoint + '/users/favorites',
        data: {
          raceId: race.idCarrera,
          username  : user.username,
          fbToken: user.fbData.accessToken
        }
      }).then(data => resolve(data.data), reject);
    });
  };

  service.removeFavorite = (user, race) => {
    return new Promise((resolve, reject) => {
      $http({
        method: 'DELETE',
        url: config.endpoint + '/users/favorites',
        data: {
          raceId: race.idCarrera,
          username  : user.username,
          fbToken: user.fbData.accessToken
        }
      }).then(data => resolve(data.data), reject);
    });
  };

  return service;
}

export default {
  name: 'RaceService',
  fn: RaceService
};
