import _ from 'lodash';
import Rx from 'rx';

function UserService($rootScope, $http, config, ErrorHandler, $q, $window) {
  'ngInject';

  const service = {};
  
  // Load FB SDK
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_LA/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
  $window.fbAsyncInit = () => {
    console.log('FB async init');

    FB.init({
      appId: config.fbAppId,
      status: true,
      cookie: true,
      xfbml: true,
      version: 'v2.6'
    });

    FB.Event.subscribe('auth.login', function(response) {
      // do something with response
      console.log("FB login: " + JSON.stringify(response));
    });

    FB.Event.subscribe('auth.logout', function(response){
      // Do something to let your program know that the user is now logged out.
      console.log("FB logout: " + JSON.stringify(response));
    });
    
    service.updateLoginStatus();
    // Fetch user info as soon as app starts
    service.getCurrentUser().subscribe(user => {
      console.log(JSON.stringify(user));
    }, error => {
      console.log(JSON.stringify(error));
    });
  };

  // for fb login button
  $window.onFacebookLogin = () => {
    $rootScope.user = null;
    
    service.getCurrentUser().subscribe(user => {
      console.log(JSON.stringify(user));
    }, error => {
      console.log(JSON.stringify(error));
    });
  };

  $rootScope.user = null;
  
  var fbSubject = new Rx.ReplaySubject(1);
  
  service.getFbUser = (forceRefresh) => {
    if (forceRefresh) service.updateLoginStatus();
    return fbSubject;
  };
  
  service.updateLoginStatus = (force) => {
    if (typeof FB == 'undefined' || _.isNil(FB)) {
      console.log('FB not yet ready');
      return;
    }
    
    console.log('Updating FB login status');
    FB.getLoginStatus(loginStatusResponse => {
      var oldSubject = fbSubject;
      fbSubject = new Rx.ReplaySubject(1);
      
      //debugger;
      console.log(JSON.stringify(loginStatusResponse));
      if (loginStatusResponse.status === 'connected') {
        console.log('Updating FB user data');
        FB.api('/me', {
          fields: 'first_name, last_name, name, email, picture'
        }, response => {
            if (!response || response.error) {
              oldSubject.onError(response);
              fbSubject.onError(response);
            } else {
              response.accessToken = loginStatusResponse.authResponse.accessToken;
              oldSubject.onNext(response);
              oldSubject.onCompleted();
              
              fbSubject.onNext(response);
              fbSubject.onCompleted();
            }
        });
      } else {
        oldSubject.onError({
          error: 'No user logged in'
        });
        fbSubject.onError({
          error: 'No user logged in'
        });
      }
      //oldSubject.dispose();
    }, force);
  };

  service.getCachedUser = () => {
    return $rootScope.user;
  };

  $rootScope.loginPending = true;
  
  var currentUserOp;
  
  service.getCurrentUser = (forceRefresh) => {
    if ($rootScope.user) {
      console.log('cached user');
      return Rx.Observable.just($rootScope.user);
    }
    console.log('no cached user');
    if (!currentUserOp) {
      currentUserOp = service.getFbUser(forceRefresh)
        .flatMap(fbUser => {
          console.log('fetching be data');
          return $http({
              method: 'POST',
              url: config.endpoint + '/users',
              data: {
                username: fbUser.id,
                fbData: fbUser
              }
            }).then(data => {
              var user = data.data.user;
              user.existent = data.data.existent;
              return user;
            });
        })
        .doOnNext(user => { // custom user
          $rootScope.user = user;
          $rootScope.$broadcast('login', user);
        })
        .doOnCompleted(() => {
          $rootScope.loginPending = false;
          currentUserOp = null;
          $rootScope.safeApply();
        })
        .doOnError(error => { // error in stream chain (could be FB or BE)
          $rootScope.loginPending = false;
          currentUserOp = null;
          $rootScope.safeApply();
        }).share(); // share to avoid concurrent subscriptions
    }
    return currentUserOp;
  };

  service.login = () => {
    return new Promise((resolve, reject) => {
      FB.login(response => {
        console.log(JSON.stringify(response));
        service.updateLoginStatus();
        service.getCurrentUser().subscribe(resolve, reject);
      }, {
        perms: 'email,publish_stream,public_profile',
        scope: 'email,public_profile,user_friends'//,user_likes,publish_actions
      });
    });
  };

  service.logout = () => {
    service.updateLoginStatus(true);
    fbSubject.subscribe(user => {
      FB.logout(response => {
        console.log(JSON.stringify(response));
        service.updateLoginStatus(true);
      });
      $rootScope.$broadcast('logout', $rootScope.user);
      $rootScope.user = null;
      $rootScope.safeApply(); // force digest loop since callback is called outside context
    }, error => {
      $rootScope.$broadcast('logout', $rootScope.user);
      $rootScope.user = null;
      $rootScope.safeApply(); // force digest loop since callback is called outside context
    });
  };

  return service;
};

export default {
  name: 'UserService',
  fn: UserService
};
