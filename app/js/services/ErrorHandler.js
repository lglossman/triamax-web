function ErrorHandler($http, config, $translate) {
  'ngInject';

  const service = {};

  service.handle = (e, prefix) => {
    var msg, error;
    if (e.data) {
        error = e.data.error;
        if (!error) error = e.data.code;
    } else {
        if (e.status === -1) {
            error = 'network_error';
        } else {
            error = 'unknown';
        }
    }

    const key = msg => {
      if (prefix) return msg + "." + prefix;
      return msg;
    };

    var msg = $translate.instant(key(error));
    if (msg == key(error)) {
      msg = $translate.instant(key('http.' + e.status));
    }
    return msg;
  };

  return service;
}

export default {
  name: 'ErrorHandler',
  fn: ErrorHandler
};
