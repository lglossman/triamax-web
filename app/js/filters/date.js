function DateFilter() {

  return function(input) {
    return input.replace(/keyboard/ig,'leopard');
  };

}

export default {
  name: 'DateFilter',
  fn: DateFilter
};